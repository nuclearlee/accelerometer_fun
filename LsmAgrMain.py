import logging as log
import os
import sys
import time
from datetime import datetime

import adafruit_lis2mdl
import adafruit_lsm303_accel
import adafruit_mpu6050
import board
import busio

from MiniPiTFTDrawing import ImageData
from SetupMiniPiTFT import EnableBacklight, DisableBacklight, DisplayButtons

# gain and offset in m/s^2
mpuOffsetTuple = [0.6475, -0.2205, -0.8870]
mpuGainTuple = [9.8175, 9.7385, 9.9970]
lsmOffsetTuple = [0.2755, -0.2135, -0.1455]
lsmGainTuple = [9.7845, 9.5165, 10.0755]


def InitMpu6050():
    i2c = busio.I2C(board.SCL, board.SDA)
    mpu = adafruit_mpu6050.MPU6050(i2c)
    return mpu


def InitLsm303agr():
    # lsm only takes 0.007s to retrieve data
    # but my calibration doesn't seem as accurate
    i2c = busio.I2C(board.SCL, board.SDA)
    mag = adafruit_lis2mdl.LIS2MDL(i2c)
    accel = adafruit_lsm303_accel.LSM303_Accel(i2c)
    return mag, accel


def GetMpuTuple(mpuDevice):
    # start = time.perf_counter()
    accel = mpuDevice.acceleration
    gyro = mpuDevice.gyro
    temp = mpuDevice.temperature
    # stop = time.perf_counter()
    # it takes about 0.016s to retrieve the values
    # how much does this affect the time steps?
    # print("mpu retrievaltime: %0.7f" % (stop - start))
    return accel, gyro, temp


def GetLsmTuple(mag, accel):
    magVals = mag.magnetic
    accelVals = accel.acceleration
    return accelVals, magVals


def GetAccelerationMagnitude(accelTuple):
    totalSquared = 0.0
    for dim in accelTuple:
        totalSquared += dim ** 2
    accelerationMagnitude = totalSquared ** 0.5
    return accelerationMagnitude


def GetAverageEqualIntervals(previousAverage, currentValue, currentSegmentNumber):
    currentAverage = (previousAverage * (currentSegmentNumber - 1) + currentValue) / currentSegmentNumber
    return currentAverage


def GetAverageVaryingIntervals(previousAverage, currentValue, segmentLength, totalLength):
    currentAverage = (previousAverage * (totalLength - segmentLength) + currentValue) / totalLength
    return currentAverage


def GetCalibratedAccelerationInG(currentAcceleration, offsetVector, gainVector):
    calibratedAcceleration = [0.0, 0.0, 0.0]
    i = 0
    for val in currentAcceleration:
        calibratedAcceleration[i] = (val - offsetVector[i]) / gainVector[i]
        i = i + 1
    return calibratedAcceleration


# it takes about 0.2s to update the screen do it sparingly
def ScreenUpdater(totalFallCount, currentFallNumber, fallHeightFromRest,
                  fallTimeUpdate, imageData, maxAcceleration, maxFallHeight):
    log.info("screen updating")
    imageData.ClearScreen()
    accel_header = "Max G  |  Max H"
    color = (255, 0, 0)
    imageData.DrawNewLine(accel_header, color)
    maxes = "%0.1f g  |  %0.1f cm" % (maxAcceleration, maxFallHeight)
    color = (0, 0, 255)
    imageData.DrawNewLine(maxes, color)
    fallStats = "H_f,H_t: "
    color = (0, 255, 200)
    imageData.DrawNewLine(fallStats, color)
    imageData.DrawNewLine("[%d]: %0.1f cm, %0.3f s" % (currentFallNumber, fallHeightFromRest, fallTimeUpdate), color)
    imageData.DrawNewLine("fall count: %d" % totalFallCount, color)
    imageData.disp.image(imageData.image, imageData.rotation)


def main():
    # setup the display and image
    imageData = ImageData()
    displayButtons = DisplayButtons(imageData.disp)
    imageData.ClearScreen()
    EnableBacklight()
    backlightOn = True
    imageData.RotateScreen270Degrees()

    timeStart = time.perf_counter()
    mpu = InitMpu6050()
    mpu.accelerometer_range = adafruit_mpu6050.Range.RANGE_16_G
    mpu.cycle_rate = adafruit_mpu6050.Rate.CYCLE_40_HZ
    mpuCycleRate = 40
    mpuCycleTime = 1.0 / mpuCycleRate
    averageMpuRetrievalTime = 0.016  # seconds
    log.info("range %d" % mpu.accelerometer_range)
    log.info("cycle_rate: %d " % mpu.cycle_rate)
    log.info("temperature: %d " % mpu.temperature)
    log.info("bandwidth: %d " % mpu.filter_bandwidth)

    # lsmMag, lsmAccel = InitLsm303agr()
    # lsmAccel.mode = adafruit_lsm303_accel.Mode.MODE_HIGH_RESOLUTION
    # log.info("data_rate: %d" % lsmAccel.data_rate)
    initTime = time.perf_counter() - timeStart
    log.info("init time: " + str(initTime))

    accelerationTuple = [0.0, 0.0, 0.0]
    jumpStats = dict()
    jumpStats = {"minG": 1.0,
                 "hangTime": 1.0,
                 "fallHeight": 1.0}
    last10Jumps = []
    last10Jumps.append({"minG": 1.0, "hangTime": 1.0, "fallHeight": 1.0})
    i = 0

    totalSeconds = 0.0

    # just trying a 25% multiplier, 100 cm drops are around 80 cm
    FALL_HEIGHT_CORRECTION_FACTOR = 1.24
    MIN_FALL_ACCELERATION = 0.2
    MIN_FALL_HEIGHT = 25  # cm. Don't count falls below this height
    BARE_MINIMUM_FALL_HEIGHT = 10  # cm. I guess display to show how lame you are
    MIN_TIME_BETWEEN_JUMPS = 0.5  # seconds. Don't count falls that occur between this time interval
    mpuTimeInterval = 0.0
    fallSteps = 0
    fallTime = 0.0
    fallTimeUpdate = 0.0
    fallHeightFromRest = 0.0
    lastUpdateTimer = 0.0
    timeStart = 0.0
    currentMinAccelMagnitude = 1.0
    maxAcceleration = 0.0
    maxFallHeight = 0.0
    screenUpdate = True
    fallingNow = False
    lastFallRecorded = time.perf_counter()
    fallCounter = 0
    lastButtonPress = time.perf_counter()
    screenTime = 0.0
    fallStart = time.perf_counter()
    i = 0
    currentFallNumber = 0
    fallDisplayerNumber = currentFallNumber
    while True:
        if displayButtons.buttonB.value and not displayButtons.buttonA.value:
            screenUpdate = True
            fallCounter = 0
            fallHeightFromRest = 0.0
            maxAcceleration = 0.0
            maxFallHeight = 0.0
            log.info("**resetting display** " + str(datetime.now()))
        if displayButtons.buttonA.value and not displayButtons.buttonB.value:
            log.info("only button a")
            # scroll backward from most recent jump
            if fallDisplayerNumber >= len(last10Jumps):
                fallDisplayerNumber = len(last10Jumps) - 1
            ScreenUpdater(fallCounter, fallDisplayerNumber, last10Jumps[fallDisplayerNumber]["fallHeight"],
                          last10Jumps[fallDisplayerNumber]["hangTime"], imageData, maxAcceleration, maxFallHeight)
            if fallDisplayerNumber == 0:
                fallDisplayerNumber = len(last10Jumps) - 1
            else:
                fallDisplayerNumber -= 1

        if not displayButtons.buttonA.value and not displayButtons.buttonB.value:
            if (time.perf_counter() - lastButtonPress) > 0.3:
                lastButtonPress = time.perf_counter()
                if not backlightOn:
                    log.info(str(datetime.now()) + "enabling backlight")
                    EnableBacklight()
                    backlightOn = True
                    ScreenUpdater(fallCounter, currentFallNumber, last10Jumps[currentFallNumber]["fallHeight"],
                                  last10Jumps[currentFallNumber]["hangTime"], imageData, maxAcceleration, maxFallHeight)
                elif backlightOn:
                    log.info("disabling backlight")
                    DisableBacklight()
                    backlightOn = False

        loopStart = time.perf_counter()
        accelerationTuple = GetMpuTuple(mpu)
        # accelerationTuple = GetLsmTuple(lsmMag,lsmAccel)
        accelGetTime = time.perf_counter()

        calibratedAcceleration = GetCalibratedAccelerationInG(accelerationTuple[0], mpuOffsetTuple, mpuGainTuple)
        # calibratedAcceleration = GetCalibratedAccelerationInG(accelerationTuple[0], lsmOffsetTuple, lsmGainTuple)
        accelerationMagnitude = GetAccelerationMagnitude(calibratedAcceleration)
        # uncomment to check calibration
        # log.info("|%0.2f|,%0.2f,%0.2f,%0.2f" % (accelerationMagnitude,
        #                                            calibratedAcceleration[0], calibratedAcceleration[1],
        #                                            calibratedAcceleration[2]))
        sensorRetrievalTime = time.perf_counter()
        if accelerationMagnitude > maxAcceleration:
            maxAcceleration = accelerationMagnitude

        if accelerationMagnitude > 0.5 and fallSteps > 0:
            fallSteps = 0
            fallTime = 0.0

        timeBetweenZeros = time.perf_counter() - lastFallRecorded

        if accelerationMagnitude < MIN_FALL_ACCELERATION and \
                (timeBetweenZeros > MIN_TIME_BETWEEN_JUMPS or fallingNow):
            falltimetimer = 0.0
            if fallingNow:
                falltimetimer = time.perf_counter() - fallStart
            else:
                fallStart = time.perf_counter()
            fallingNow = True
            # fallingNow = mpuMagnitude < lastAccelMagnitude
            lastAccelMagnitude = accelerationMagnitude
            if accelerationMagnitude < currentMinAccelMagnitude:
                currentMinAccelMagnitude = accelerationMagnitude
            screenUpdate = False  # don't update while falling, unless you want to use this skydiving or something
            fallSteps += 1
            stepTime = sensorRetrievalTime - timeStart - screenTime
            fallTime += stepTime
            fallTimeUpdate = falltimetimer

            # without speed and angle, can't really calculate the fall height or trajectory
            # a -> 1.0 as you leave the ground (or as the device leaves your hand if you're throwing it around)
            fallHeightFromRest = FALL_HEIGHT_CORRECTION_FACTOR * 0.5 * 980.0 * falltimetimer ** 2
            log.info("(t,h_f)(|a|,ax,ay,az): %0.4f,%0.1f,|%0.2f|,%0.2f,%0.2f,%0.2f" % (fallTime, fallHeightFromRest,
                                                                                       accelerationMagnitude,
                                                                                       calibratedAcceleration[0],
                                                                                       calibratedAcceleration[1],
                                                                                       calibratedAcceleration[2]))

        elif accelerationMagnitude < 0.2 and timeBetweenZeros < 0.5:
            log.warning("bounce!")
        elif fallingNow:
            fallingNow = False
            # this saves me from having to detect whether or not I'm actually falling without
            # losing sensitivity
            if fallHeightFromRest >= MIN_FALL_HEIGHT:
                screenUpdate = True
                fallCounter += 1
                lastFallRecorded = time.perf_counter()
                if fallHeightFromRest > maxFallHeight:
                    maxFallHeight = fallHeightFromRest
            else:
                log.info("fall not counted: %0.1f" % fallHeightFromRest)
            if currentMinAccelMagnitude < 0.1 and fallHeightFromRest > BARE_MINIMUM_FALL_HEIGHT:
                screenUpdate = True
                # not counting the fall, but still need to assume a gap
                # between jumps/falls otherwise the screen update makes it look like it just
                # fell 20 cm
                lastFallRecorded = time.perf_counter()
                if len(last10Jumps) > 9:
                    last10Jumps.pop(0)
                    currentFallNumber = 9
                else:
                    currentFallNumber += 1

                last10Jumps.append({"minG":accelerationMagnitude, "hangTime":fallTimeUpdate, "fallHeight":fallHeightFromRest})
            log.info("end fall!")
        timeStart = time.perf_counter()

        if screenUpdate:
            if backlightOn:
                drawStart = time.perf_counter()
                ScreenUpdater(fallCounter, currentFallNumber, last10Jumps[currentFallNumber]["fallHeight"],
                              last10Jumps[currentFallNumber]["hangTime"], imageData, maxAcceleration, maxFallHeight)
                screenTime = time.perf_counter() - drawStart
            lastUpdateTimer = 0.0
            screenUpdate = False

        loopEnd = time.perf_counter()

        loopTime = loopEnd - loopStart - screenTime
        screenTime = 0.0
        totalSeconds += loopTime
        i += 1
        mpuTimeInterval = GetAverageEqualIntervals(mpuTimeInterval, loopTime, i)
        if not screenUpdate:
            lastUpdateTimer += loopTime


if __name__ == "__main__":
    log.basicConfig(filename='/home/pi/accelerometer_log.log', level=log.DEBUG, format='%(asctime)s %(message)s')
    log.info("Initializing log")
    try:
        main()
    except KeyboardInterrupt:
        DisableBacklight()
        try:
            log.warning("KeyboardInterrupt caught, exiting")
            sys.exit("keyboard interrupt caught")
        except:
            log.error("sys.exit(1) did not work unable to exit!")
